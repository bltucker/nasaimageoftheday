package bltucker.dev.nasaimageoftheday.domainservices;

import android.graphics.Bitmap;
import android.util.LruCache;

public final class ImageCache {

    private static ImageCache applicationCache;

    public synchronized final static ImageCache getApplicationCache(){

        if(null == applicationCache){
            applicationCache = new ImageCache();
        }

        return applicationCache;
    }



    private final LruCache<Integer, Bitmap> cache;

    private final int maxMemory;
    private final int cacheSize;


    public ImageCache() {
        maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        cacheSize = maxMemory / 8;

        cache = new LruCache<Integer, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(Integer key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes.
                return bitmap.getByteCount() / 1024;
            }
        };
    }



    public synchronized void addBitmap(Integer key, Bitmap bitmap) {
        if (null == getBitmap(key)) {
            cache.put(key, bitmap);
        }
    }



    public Bitmap getBitmap(Integer key) {
        return cache.get(key);
    }
}
