package bltucker.dev.nasaimageoftheday.domainservices;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import bltucker.dev.nasaimageoftheday.utilities.SafeCloser;
import bltucker.dev.nasaimageoftheday.utilities.StreamConverter;

public final class DownloadTask {

    private static final int MAX_IMAGE_WIDTH = 200;
    private static final int MAX_IMAGE_HEIGHT = 200;

    private static final SafeCloser streamCloser = new SafeCloser();
    private static final StreamConverter streamConverter = new StreamConverter();

    private final String downloadUrl;


    public DownloadTask(String downloadUri){
        this.downloadUrl = downloadUri;
    }



    public Bitmap download(){

        InputStream responseStream = null;
        HttpURLConnection connection = null;

        try{

            URL url = new URL(downloadUrl);
            connection = (HttpURLConnection) url.openConnection();

            responseStream = connection.getInputStream();
            byte[] imageBytes = streamConverter.convertToByteArray(responseStream);


            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

            bitmapOptions.inJustDecodeBounds = true;

            BitmapFactory.decodeByteArray(imageBytes,0,imageBytes.length, bitmapOptions);

            bitmapOptions.inSampleSize = this.calculateInSampleSize(bitmapOptions, MAX_IMAGE_WIDTH, MAX_IMAGE_HEIGHT);

            bitmapOptions.inJustDecodeBounds = false;

            return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, bitmapOptions);

        } catch(Exception ex){

        } finally {

            streamCloser.closeSafely(responseStream);
            if(connection != null){
                connection.disconnect();
            }
        }

        return null;
    }



    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
