package bltucker.dev.nasaimageoftheday.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import bltucker.dev.nasaimageoftheday.R;
import bltucker.dev.nasaimageoftheday.adapters.ImageOfTheDayAdapter;
import bltucker.dev.nasaimageoftheday.asynctasks.NasaImageOfTheDayRSSGrabber;
import bltucker.dev.nasaimageoftheday.datamodels.NasaRSS;


//more feeds
//http://apod.nasa.gov/apod.rss
//http://www.w3wallpapers.com/rss/munising_falls_trail_alger_county_michigan__1-wallpapers
//http://www.botanicalgarden.ubc.ca/potd/atom.xml

public final class GalleryActivity extends Activity implements NasaImageOfTheDayRSSGrabber.Delegate {

    private final NasaImageOfTheDayRSSGrabber rssGrabber = new NasaImageOfTheDayRSSGrabber();

    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        this.gridView = (GridView) findViewById(R.id.gridView);
        this.gridView.setAdapter(new ImageOfTheDayAdapter(this, new ArrayList<String>()));

        rssGrabber.setDelegate(this);
        rssGrabber.execute();
    }


    @Override
    protected  void onPause(){
        super.onPause();
        if(isFinishing()){
            rssGrabber.setDelegate(null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void sendResults(List<String> results) {
        this.getImageOfTheDayAdapter().updateList(results);
    }


    private ImageOfTheDayAdapter getImageOfTheDayAdapter(){
       return (ImageOfTheDayAdapter)  this.gridView.getAdapter();
    }
}
