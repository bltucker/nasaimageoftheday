package bltucker.dev.nasaimageoftheday.asynctasks;


import android.os.AsyncTask;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import bltucker.dev.nasaimageoftheday.datamodels.NasaRSS;
import bltucker.dev.nasaimageoftheday.domainservices.NasaRSSParser;
import bltucker.dev.nasaimageoftheday.utilities.SafeCloser;
import bltucker.dev.nasaimageoftheday.utilities.StreamConverter;

public final class NasaImageOfTheDayRSSGrabber extends AsyncTask<Void, Void, List<String>> {


    public static interface Delegate{
        void sendResults(List<String> results);
    }

    private static final String RSS_URL = "http://www.nasa.gov/rss/dyn/image_of_the_day.rss";

    private static StreamConverter streamConverter = new StreamConverter();
    private static SafeCloser streamCloser = new SafeCloser();

    private Delegate delegate;


    public void setDelegate(Delegate delegate){
        this.delegate = delegate;
    }


    public void removeDelegate(){
        this.delegate = null;
    }


    @Override
    protected List<String> doInBackground(Void... params) {

        InputStream responseStream = null;
        HttpURLConnection connection = null;
        List<String> urls = new ArrayList<String>();

        try{

            URL url = new URL(RSS_URL);
            connection = (HttpURLConnection) url.openConnection();

            responseStream = connection.getInputStream();
            NasaRSSParser parser = new NasaRSSParser();
            NasaRSS rssFeed = parser.parse(responseStream);


            for(int i = 0; i < rssFeed.size(); i++){
                urls.add(rssFeed.get(i).url);
            }

        } catch(Exception ex){

        } finally {

            streamCloser.closeSafely(responseStream);

            if(connection != null){
                connection.disconnect();
            }

        }

        return urls;
    }


    @Override
    protected void onPostExecute(List<String> result){
        if(null != this.delegate && null != result){
            this.delegate.sendResults(result);
        }
    }
}
