package bltucker.dev.nasaimageoftheday.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.List;

import bltucker.dev.nasaimageoftheday.androidservices.ImageDownloader;
import bltucker.dev.nasaimageoftheday.domainservices.ImageCache;

public final class ImageOfTheDayAdapter extends BaseAdapter {

    private static final class BitmapHandler extends Handler {

        private final WeakReference<ImageView> imageViewRef;

        public BitmapHandler(ImageView imageView){
            imageViewRef = new WeakReference<ImageView>(imageView);
        }


        @Override
        public void handleMessage(Message message){

            requestedImages.remove(message.arg1);

            if(shouldSetImageViewBitmap(message)){
                Bitmap image = (Bitmap) message.obj;
                ImageCache.getApplicationCache().addBitmap(message.arg1, image);
                imageViewRef.get().setImageBitmap(image);
            }
        }


        private boolean shouldSetImageViewBitmap(Message message){
            return message.what == ImageDownloader.SUCCESSFUL &&
                    imageViewRef.get() != null &&
                    imageViewRef.get().getTag().hashCode() == message.arg1 &&
                    message.obj != null;
        }

    }

    private final static HashSet<Integer> requestedImages = new HashSet<Integer>();

    private ImageCache imageCache = ImageCache.getApplicationCache();
    private final Context ctx;
    private final List<String> dataList;

    public ImageOfTheDayAdapter(Context ctx, List<String> data){
        this.ctx = ctx;
        this.dataList = data;
    }


    public void updateList(List<String> newStrings){
        this.dataList.addAll(newStrings);
        this.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = initializeNewImageView();
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageBitmap(null);

        String imageUrl = this.dataList.get(position);
        imageView.setTag(imageUrl);

        Bitmap image = imageCache.getBitmap(imageUrl.hashCode());
        if(null != image){
            imageView.setImageBitmap(image);
        } else {
            this.notifyImageDownloadService(imageView, imageUrl);
        }

        return imageView;
    }


    private ImageView initializeNewImageView(){
        ImageView imageView = new ImageView(this.ctx);
        imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(8, 8, 8, 8);

        return imageView;
    }


    private void notifyImageDownloadService(ImageView imageView, String url){
        requestedImages.add(url.hashCode());
        BitmapHandler handler = new BitmapHandler(imageView);
        Messenger messenger = new Messenger(handler);
        ImageDownloader.SendDownloadRequest(url, ctx, messenger);
    }
}
