package bltucker.dev.nasaimageoftheday.utilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class StreamConverter {



    public byte[] convertToByteArray(InputStream in) throws IOException {

        ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream();
        byte[] readBuffer = new byte[1024];
        int bytesRead;
        while((bytesRead = in.read(readBuffer)) > -1){
            outputBuffer.write(readBuffer, 0, bytesRead);
            outputBuffer.flush();
        }

        return outputBuffer.toByteArray();
    }


    public String convertToString(InputStream in) throws IOException {
        byte[] stringBytes = this.convertToByteArray(in);
        return new String(stringBytes);
    }
}

