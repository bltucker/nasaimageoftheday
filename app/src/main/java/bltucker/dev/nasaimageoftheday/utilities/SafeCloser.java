package bltucker.dev.nasaimageoftheday.utilities;

import java.io.Closeable;

public final class SafeCloser {


    public void closeSafely(Closeable... closeables){
        for(Closeable c : closeables){
            try{
                c.close();
            } catch(Exception ex) { /* ignored */ }
        }
    }
}
