package bltucker.dev.nasaimageoftheday.androidservices;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import bltucker.dev.nasaimageoftheday.domainservices.DownloadTask;

public final class ImageDownloader extends Service {

    public static final String DOWNLOAD_FROM_URL = "from_url";
    public static final String MESSENGER = "messenger";

    public static final int SUCCESSFUL = "download_successful".hashCode();
    public static final int FAILED = "download_failed".hashCode();


    private final Executor threadPool = Executors.newFixedThreadPool(5);

    public static void SendDownloadRequest(String imageUrl, Context context, Messenger messenger){
        Intent downloadIntent = new Intent(context, ImageDownloader.class);
        downloadIntent.putExtra(DOWNLOAD_FROM_URL, imageUrl);
        downloadIntent.putExtra(MESSENGER, messenger);
        context.startService(downloadIntent);
    }


    public ImageDownloader() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

       final String downloadUrl = intent.getStringExtra(DOWNLOAD_FROM_URL);
       final Messenger messenger = intent.getParcelableExtra(MESSENGER);

        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                try{
                    DownloadTask dt = new DownloadTask(downloadUrl);
                    Bitmap image = dt.download();
                    if(null != image){
                        messenger.send(Message.obtain(null, SUCCESSFUL, downloadUrl.hashCode(),0, image));
                    } else {
                        messenger.send(Message.obtain(null, FAILED, downloadUrl.hashCode(), 0));
                    }
                } catch(Exception ex){
                    /* ignored */
                }
            }
        });


        return START_STICKY;
    }
}
